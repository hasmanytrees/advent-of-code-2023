package Day5

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func Star2() {
	input, _ := os.Open("Day5/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	location := math.MaxInt

	ala := parseAlmanac(scanner)

	seedIntervals := []interval{}

	for i := 0; i < len(ala.seeds)-1; i = i + 2 {
		start := ala.seeds[i]
		end := ala.seeds[i] + ala.seeds[i+1]

		seedIntervals = append(seedIntervals, interval{start, end})
	}

	for i := 0; i < math.MaxInt; i++ {
		seed := ala.getSeed(i)

		for _, seedInterval := range seedIntervals {
			if seedInterval.start <= seed && seed <= seedInterval.end {
				location = i
				break
			}
		}

		if location < math.MaxInt {
			break
		}
	}

	fmt.Println("2520479")
	fmt.Println(location)
}

func (a *almanac) getSeed(i int) int {
	result := i

	mapDestination := "location"
	// fmt.Printf("%s %d,", mapDestination, result)

	for {
		if m, ok := a.seedMapsReverse[mapDestination]; ok {
			result = m.getSource(result)

			// fmt.Printf("%s %d,", m.source, result)

			mapDestination = m.source
		} else {
			break
		}
	}

	// fmt.Println()

	return result
}

type interval struct {
	start int
	end   int
}
