package Day5

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func Star1() {
	input, _ := os.Open("Day5/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	location := math.MaxInt

	ala := parseAlmanac(scanner)

	for _, seed := range ala.seeds {
		location = min(location, ala.getLocation(seed))
	}

	fmt.Println("462648396")
	fmt.Println(location)
}

type seedMap struct {
	source      string
	destination string
	ranges      []*mapRange
}

type mapRange struct {
	destinationStart int
	sourceStart      int
	length           int
}

func (s *seedMap) getDestination(i int) int {
	result := i

	for _, r := range s.ranges {
		if r.sourceStart <= i && i <= r.sourceStart+r.length {
			result = r.destinationStart + i - r.sourceStart
			break
		}
	}

	return result
}

func (s *seedMap) getSource(i int) int {
	result := i

	for _, r := range s.ranges {
		if r.destinationStart <= i && i <= r.destinationStart+r.length {
			result = r.sourceStart + i - r.destinationStart
			break
		}
	}

	return result
}

type almanac struct {
	seeds           []int
	seedMaps        map[string]*seedMap
	seedMapsReverse map[string]*seedMap
}

func parseAlmanac(scanner *bufio.Scanner) *almanac {
	result := &almanac{
		seeds:           []int{},
		seedMaps:        map[string]*seedMap{},
		seedMapsReverse: map[string]*seedMap{},
	}

	scanner.Scan()

	seedsLine := scanner.Text()
	seedsRegexp := regexp.MustCompile(`\d+`)

	for _, a := range seedsRegexp.FindAllStringSubmatch(seedsLine, -1) {
		i, _ := strconv.Atoi(a[0])
		result.seeds = append(result.seeds, i)
	}

	scanner.Scan()

	for scanner.Scan() {
		mapRegexp := regexp.MustCompile(`(\w+)-to-(\w+) map:`)
		seedMapLine := scanner.Text()

		seedMapParts := mapRegexp.FindAllStringSubmatch(seedMapLine, -1)
		source := seedMapParts[0][1]
		destination := seedMapParts[0][2]
		seedMap := &seedMap{
			source:      source,
			destination: destination,
			ranges:      []*mapRange{},
		}
		result.seedMaps[source] = seedMap
		result.seedMapsReverse[destination] = seedMap

		for scanner.Scan() {
			mapLine := scanner.Text()

			if len(mapLine) == 0 {
				break
			}

			r := &mapRange{}

			mapLineRegexp := regexp.MustCompile(`\d+`)
			mapLineParts := mapLineRegexp.FindAllStringSubmatch(mapLine, -1)

			r.destinationStart, _ = strconv.Atoi(mapLineParts[0][0])
			r.sourceStart, _ = strconv.Atoi(mapLineParts[1][0])
			r.length, _ = strconv.Atoi(mapLineParts[2][0])

			seedMap.ranges = append(seedMap.ranges, r)
		}
	}

	return result
}

func (a *almanac) getLocation(i int) int {
	result := i

	mapSource := "seed"
	fmt.Printf("%s %d,", mapSource, result)

	for {
		if m, ok := a.seedMaps[mapSource]; ok {
			result = m.getDestination(result)

			fmt.Printf("%s %d,", m.destination, result)

			mapSource = m.destination
		} else {
			break
		}
	}

	fmt.Println()

	return result
}
