package Day7

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func Star1() {
	input, _ := os.Open("Day7/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	hands := []*hand{}

	for scanner.Scan() {
		h := parseHand(scanner.Text())
		h.calcValue()
		hands = append(hands, h)
	}

	sort.SliceStable(hands, func(i, j int) bool {
		return hands[i].value < hands[j].value
	})

	for i, hand := range hands {
		hand.winnings += hand.bid * (i + 1)
	}

	winnings := 0

	for _, hand := range hands {
		winnings += hand.winnings
	}

	fmt.Println("253866470")
	fmt.Println(winnings)
}

type hand struct {
	cards            string
	cardinalities    map[byte]int
	cardDistribution map[int]int
	bid              int
	value            string
	winnings         int
	numJokers        int
}

func parseHand(l string) *hand {
	result := &hand{
		cardinalities:    map[byte]int{},
		cardDistribution: map[int]int{},
	}

	result.cards = l[0:5]

	for i := 0; i < 5; i++ {
		result.cardinalities[l[i]]++
	}

	for _, v := range result.cardinalities {
		result.cardDistribution[v]++
	}

	result.bid, _ = strconv.Atoi(l[6:])

	return result
}

func (h *hand) calcValue() {
	_, fiveOfAKind := h.cardDistribution[5]
	_, fourOfAKind := h.cardDistribution[4]
	_, threeOfAKind := h.cardDistribution[3]
	numPairs := h.cardDistribution[2]
	twoPair := numPairs > 1
	onePair := !twoPair && numPairs > 0
	fullHouse := threeOfAKind && onePair

	value := ""

	// 54F321H12345
	if fiveOfAKind {
		value += "1"
	} else {
		value += "0"
	}

	if fourOfAKind {
		value += "1"
	} else {
		value += "0"
	}

	if fullHouse {
		value += "1"
	} else {
		value += "0"
	}

	if threeOfAKind {
		value += "1"
	} else {
		value += "0"
	}

	if twoPair {
		value += "1"
	} else {
		value += "0"
	}

	if onePair {
		value += "1"
	} else {
		value += "0"
	}

	if !fiveOfAKind && !fourOfAKind && !fullHouse && !threeOfAKind && !twoPair && !onePair {
		value += "1"
	} else {
		value += "0"
	}

	for _, c := range h.cards {
		switch c {
		case 'A':
			value += "E"
		case 'K':
			value += "D"
		case 'Q':
			value += "C"
		case 'J':
			value += "B"
		case 'T':
			value += "A"
		case '9':
			value += "9"
		case '8':
			value += "8"
		case '7':
			value += "7"
		case '6':
			value += "6"
		case '5':
			value += "5"
		case '4':
			value += "4"
		case '3':
			value += "3"
		case '2':
			value += "2"
		}
	}

	h.value = value
}
