package Day6

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func Star1() {
	input, _ := os.Open("Day6/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	scanner.Scan()
	times := parseNumbersFromLine(scanner.Text())
	scanner.Scan()
	distances := parseNumbersFromLine(scanner.Text())

	races := []*race{}

	for i := range times {
		races = append(races, newRace(times[i], distances[i]))
	}

	product := 1

	for _, race := range races {
		n := race.calcNumWaysToWin()
		product *= n
	}

	fmt.Println("220320")
	fmt.Println(product)
}

func parseNumbersFromLine(line string) []int {
	result := []int{}

	re := regexp.MustCompile(`\d+`)

	for _, match := range re.FindAllStringSubmatch(line, -1) {
		n, _ := strconv.Atoi(match[0])
		result = append(result, n)
	}

	return result
}

type race struct {
	time           int
	distanceToBeat int
}

func newRace(time int, distance int) *race {
	return &race{time, distance}
}

func (r *race) calcOptimumHoldTime() int {
	return int(r.time / 2)
}

func (r *race) calcNumWaysToWin() int {
	result := 0

	holdTime := r.calcOptimumHoldTime()

	for i := 0; i < r.time-holdTime; i++ {
		t := holdTime + i
		d := t * (r.time - t)

		if d > r.distanceToBeat {
			result += 2
		} else {
			break
		}
	}

	result -= (r.time%2 + 1)

	return result
}
