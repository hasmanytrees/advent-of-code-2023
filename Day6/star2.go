package Day6

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func Star2() {
	input, _ := os.Open("Day6/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	scanner.Scan()
	times := parseNumbersFromLine(scanner.Text())
	scanner.Scan()
	distances := parseNumbersFromLine(scanner.Text())

	time := times[0]
	distance := distances[0]

	for i := 1; i < len(times); i++ {
		time *= int(math.Pow10(len(fmt.Sprintf("%d", times[i]))))
		time += times[i]

		distance *= int(math.Pow10(len(fmt.Sprintf("%d", distances[i]))))
		distance += distances[i]
	}

	race := &race{time, distance}

	n := race.calcNumWaysToWin()

	fmt.Println("34454850")
	fmt.Println(n)
}
