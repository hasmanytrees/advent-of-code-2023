package Day8

import (
	"bufio"
	"fmt"
	"os"
)

func Star2() {
	input, _ := os.Open("Day8/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)
	scanner.Scan()

	m := newMapp(scanner.Text())

	scanner.Scan()

	currentLocations := []string{}
	routeLengths := []int{}

	for scanner.Scan() {
		label, node := parseNode(scanner.Text())

		if label[2] == 'A' {
			currentLocations = append(currentLocations, label)
			routeLengths = append(routeLengths, 0)
		}

		m.nodes[label] = node
	}

	fmt.Printf("Starting Locations: %v\n", currentLocations)

	i := 0
	n := 0
	numZs := 0

	for numZs != len(currentLocations) {
		// numZs = 0

		d := m.directions[i]

		for l := 0; l < len(currentLocations); l++ {
			if routeLengths[l] == 0 {
				if d == 'L' {
					currentLocations[l] = m.nodes[currentLocations[l]].left
				} else {
					currentLocations[l] = m.nodes[currentLocations[l]].right
				}

				if currentLocations[l][2] == 'Z' {
					routeLengths[l] = n + 1
					numZs++
				}
			}
		}

		n++
		i++

		if i == len(m.directions) {
			i = 0
		}

		if n%1000000 == 0 {
			fmt.Print(".")
		}
	}

	x := LCM(routeLengths[0], routeLengths[1], routeLengths[2:]...)

	fmt.Printf("Ending Locations: %v\n", currentLocations)
	fmt.Printf("Route Lengths: %v\n", routeLengths)
	fmt.Println("15726453850399")
	fmt.Println(x)
}

// greatest common divisor (GCD) via Euclidean algorithm
func GCD(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

// find Least Common Multiple (LCM) via GCD
func LCM(a, b int, integers ...int) int {
	result := a * b / GCD(a, b)

	for i := 0; i < len(integers); i++ {
		result = LCM(result, integers[i])
	}

	return result
}
