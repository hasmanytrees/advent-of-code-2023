package Day8

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
)

type mapp struct {
	directions string
	nodes      map[string]*node
}

func newMapp(directions string) *mapp {
	return &mapp{
		directions: directions,
		nodes:      map[string]*node{},
	}
}

type node struct {
	left  string
	right string
}

func newNode(left string, right string) *node {
	return &node{
		left,
		right,
	}
}

func parseNode(s string) (string, *node) {
	r := regexp.MustCompile(`[1-9,A-Z]{3}`)

	matches := r.FindAllString(s, -1)

	return matches[0], newNode(matches[1], matches[2])
}

func Star1() {
	input, _ := os.Open("Day8/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)
	scanner.Scan()

	m := newMapp(scanner.Text())

	scanner.Scan()

	for scanner.Scan() {
		label, node := parseNode(scanner.Text())

		m.nodes[label] = node
	}

	currentLocation := "AAA"
	i := 0
	n := 0

	for currentLocation != "ZZZ" {
		d := m.directions[i]

		if d == 'L' {
			currentLocation = m.nodes[currentLocation].left
		} else {
			currentLocation = m.nodes[currentLocation].right
		}

		n++
		i++

		if i == len(m.directions) {
			i = 0
		}
	}

	fmt.Println("16043")
	fmt.Println(n)
}
