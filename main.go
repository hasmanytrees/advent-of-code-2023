package main

import (
	"flag"

	Day1 "gitlab.com/hasmanytrees/advent-of-code-2023/Day1"
	Day2 "gitlab.com/hasmanytrees/advent-of-code-2023/Day2"
	Day3 "gitlab.com/hasmanytrees/advent-of-code-2023/Day3"
	Day4 "gitlab.com/hasmanytrees/advent-of-code-2023/Day4"
	Day5 "gitlab.com/hasmanytrees/advent-of-code-2023/Day5"
	Day6 "gitlab.com/hasmanytrees/advent-of-code-2023/Day6"
	Day7 "gitlab.com/hasmanytrees/advent-of-code-2023/Day7"
	Day8 "gitlab.com/hasmanytrees/advent-of-code-2023/Day8"
)

func main() {
	day := flag.Int("d", 1, "The advent day")
	star := flag.Int("s", 1, "Which star you are solving")
	flag.Parse()

	solvers := map[int][]func(){
		1: {Day1.Star1, Day1.Star2},
		2: {Day2.Star1, Day2.Star2},
		3: {Day3.Star1, Day3.Star2},
		4: {Day4.Star1, Day4.Star2},
		5: {Day5.Star1, Day5.Star2},
		6: {Day6.Star1, Day6.Star2},
		7: {Day7.Star1, Day7.Star2},
		8: {Day8.Star1, Day8.Star2},
	}

	solvers[*day][*star-1]()
}
