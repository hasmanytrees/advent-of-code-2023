package Day4

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
)

func Star1() {
	input, _ := os.Open("Day4/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	sum := 0

	for scanner.Scan() {
		c := parseCard(scanner.Text())
		v := c.value()
		sum += v
	}

	fmt.Println("23441")
	fmt.Println(sum)
}

type card struct {
	n              int
	numMatches     int
	winningNumbers map[int]bool
	numbers        []int
}

func parseCard(a string) *card {
	c := &card{
		winningNumbers: map[int]bool{},
		numbers:        []int{},
	}

	r := regexp.MustCompile(`(?:Card +(\d+): +)((?:(?:\d+) *)*)(?: *\| *)((?:(?:\d+) *)*)`)
	submatches := r.FindStringSubmatch(a)

	numbersRegexp := regexp.MustCompile(`\d+`)

	n, _ := strconv.Atoi(submatches[1])
	c.n = n

	for _, match := range numbersRegexp.FindAllString(submatches[2], -1) {
		v, _ := strconv.Atoi(match)
		c.winningNumbers[v] = true
	}

	for _, match := range numbersRegexp.FindAllString(submatches[3], -1) {
		v, _ := strconv.Atoi(match)
		c.numbers = append(c.numbers, v)
	}

	for _, n := range c.numbers {
		if _, ok := c.winningNumbers[n]; ok {
			c.numMatches++
		}
	}

	return c
}

func (c *card) value() int {
	result := 0

	if c.numMatches > 0 {
		result = int(math.Pow(float64(2), float64(c.numMatches-1)))
	}

	return result
}
