package Day4

import (
	"bufio"
	"fmt"
	"os"
)

func Star2() {
	input, _ := os.Open("Day4/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	sum := 0
	cards := []*card{}
	numCards := map[int]int{}

	for scanner.Scan() {
		c := parseCard(scanner.Text())

		numCards[c.n]++

		if c.numMatches > 0 {
			for i := 1; i <= c.numMatches; i++ {
				numCards[c.n+i] += numCards[c.n]
			}
		}

		cards = append(cards, c)
	}

	for _, c := range cards {
		sum += numCards[c.n]
	}

	fmt.Println("5923918")
	fmt.Println(sum)
}
