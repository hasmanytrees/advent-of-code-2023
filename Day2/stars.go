package Day2

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type game struct {
	num   int
	red   int
	green int
	blue  int
}

func (g *game) power() int {
	return g.red * g.green * g.blue
}

func parseGame(a string) *game {
	// Game 1: 13 green, 3 red; 4 red, 9 green, 4 blue; 9 green, 10 red, 2 blue
	g := &game{}

	gameParts := strings.Split(a, ":")

	numReg := regexp.MustCompile(`\d+`)
	g.num, _ = strconv.Atoi(numReg.FindString(gameParts[0]))

	for _, rounds := range strings.Split(gameParts[1], ";") {
		red, green, blue := parseRounds(rounds)

		g.red = max(g.red, red)
		g.green = max(g.green, green)
		g.blue = max(g.blue, blue)
	}

	return g
}

func parseRounds(a string) (int, int, int) {
	//returns number of cubes in order of red, green, blue
	red := 0
	green := 0
	blue := 0

	rounds := strings.Split(a, ";")

	for _, p := range rounds {
		red = getNumColoredCubesForRound(p, "red")
		green = getNumColoredCubesForRound(p, "green")
		blue = getNumColoredCubesForRound(p, "blue")
	}

	return red, green, blue
}

func getNumColoredCubesForRound(round string, c string) int {
	r := regexp.MustCompile(fmt.Sprintf(`(\d+) %s`, c))

	m := r.FindStringSubmatch(round)

	result := 0

	if len(m) > 0 {
		result, _ = strconv.Atoi(m[1])
	}

	return result
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}

func Star1() {
	red := 12
	green := 13
	blue := 14

	games := []*game{}

	sum := 0

	input, _ := os.Open("Day2/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	for scanner.Scan() {
		a := scanner.Text()

		g := parseGame(a)

		if g.red <= red && g.green <= green && g.blue <= blue {
			games = append(games, g)
		}
	}

	for _, g := range games {
		sum += g.num
	}

	fmt.Println("2593")
	fmt.Println(sum)
}

func Star2() {
	games := []*game{}

	sum := 0

	input, _ := os.Open("Day2/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	for scanner.Scan() {
		a := scanner.Text()

		g := parseGame(a)

		games = append(games, g)

	}

	for _, g := range games {
		sum += g.power()
	}

	fmt.Println("54699")
	fmt.Println(sum)
}
