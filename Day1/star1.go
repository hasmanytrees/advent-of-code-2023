package Day1

import (
	"bufio"
	"fmt"
	"os"
)

func Star1() {
	input, _ := os.Open("Day1/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	sum := 0

	for scanner.Scan() {
		a := scanner.Text()

		i := findFirstNumFromLeft(a)
		j := findFirstNumFromRight(a)

		sum += (i * 10) + j
	}

	fmt.Println("54450")
	fmt.Println(sum)
}

func findFirstNumFromLeft(s string) int {
	result := 0

	for i := 0; i < len(s); i++ {
		n, ok := toNum(s[i])

		if ok {
			result = n
			break
		}
	}

	return result
}

func findFirstNumFromRight(s string) int {
	result := 0

	for i := len(s) - 1; i >= 0; i-- {
		n, ok := toNum(s[i])

		if ok {
			result = n
			break
		}
	}

	return result
}

func toNum(b byte) (int, bool) {
	if 48 <= b && b <= 57 {
		return int(b) - 48, true
	}

	return 0, false
}
