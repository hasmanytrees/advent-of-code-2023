package Day1

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func Star2() {
	input, _ := os.Open("Day1/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	sum := 0

	for scanner.Scan() {
		a := scanner.Text()

		first, last := findFirstAndLastNum(a)

		sum += (first * 10) + last
	}

	fmt.Println("54265")
	fmt.Println(sum)
}

func findFirstAndLastNum(s string) (int, int) {
	r := regexp.MustCompile(`\d|one|two|three|four|five|six|seven|eight|nine`)

	m := findAllString(r, s)

	first := stringToInt(m[0])
	last := stringToInt(m[len(m)-1])

	return first, last
}

func findAllString(r *regexp.Regexp, s string) []string {
	matches := []string{}

	offset := 0

	// have to take into account numbers that in text representation overlap
	// ie. one and eight can share an 'e'
	for {
		sub := s[offset:]
		l := r.FindStringIndex(sub)
		if l == nil {
			break
		}

		matches = append(matches, sub[l[0]:l[1]])

		// increase the offset at least 1, or the length of the match minus 1
		offset += max(1, l[1]-1)
	}

	return matches
}

func stringToInt(s string) int {
	switch s {
	case "one":
		return 1
	case "two":
		return 2
	case "three":
		return 3
	case "four":
		return 4
	case "five":
		return 5
	case "six":
		return 6
	case "seven":
		return 7
	case "eight":
		return 8
	case "nine":
		return 9
	}

	i, _ := strconv.Atoi(s)

	return i
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
