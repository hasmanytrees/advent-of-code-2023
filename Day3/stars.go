package Day3

import (
	"bufio"
	"container/ring"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

var (
	lineLength int
)

func getNumberOffsets(r *ring.Ring) [][]int {
	reg := regexp.MustCompile(`\d+`)

	m := reg.FindAllStringIndex(r.Value.(string), -1)

	return m
}

func isSymbol(b byte) bool {
	return b != '.' && (b < '0' || b > '9')
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func isPartNumber(r *ring.Ring, offset []int) bool {
	result := false

	// it's a part number if it is adjacent to a symbol that's not a period
	p := r.Prev()
	n := r.Next()

	a := max(0, offset[0]-1)
	b := min(lineLength, offset[1]+1)

	// fmt.Println("Checking for symbols")
	// if p != n {
	// 	fmt.Print("p = ")
	// 	fmt.Println(p.Value.(string)[a:b])
	// }
	// fmt.Print("r = ")
	// fmt.Println(r.Value.(string)[a:b])
	// fmt.Print("n = ")
	// fmt.Println(n.Value.(string)[a:b])

	for i := a; i < b; i++ {
		if isSymbol(p.Value.(string)[i]) {
			result = true
			break
		}
		if isSymbol(r.Value.(string)[i]) {
			result = true
			break
		}
		if n != p && isSymbol(n.Value.(string)[i]) {
			result = true
			break
		}
	}

	// fmt.Println(result)
	return result
}

func processLine(r *ring.Ring) int {
	sum := 0
	offsets := getNumberOffsets(r)

	for _, offset := range offsets {
		if isPartNumber(r, offset) {
			partNumber, _ := strconv.Atoi(r.Value.(string)[offset[0]:offset[1]])
			// fmt.Println(partNumber)
			sum += partNumber
		}
	}

	return sum
}

func Star1() {
	input, _ := os.Open("Day3/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	var r *ring.Ring

	sum := 0

	for scanner.Scan() {
		// if we don't have a ring create one then continue so we can get enough data to make decisions
		if r == nil {
			r = ring.New(1)
			r.Value = scanner.Text()
			lineLength = len(r.Value.(string))
			continue
		}

		// if we don't have 3 lines of buffer in our ring, add another ring/element
		if r.Len() < 3 {
			r.Link(ring.New(1))
		}

		// read the next line of the file into the next element
		r = r.Next()
		r.Value = scanner.Text()

		// rewind one element so we can now compare lines for adjacent symbols
		r = r.Prev()

		sum += processLine(r)

		r = r.Next()
	}

	// make sure to process the last line
	r.Next()
	r.Unlink(1)
	sum += processLine(r)

	fmt.Println("536202")
	fmt.Println(sum)
}
