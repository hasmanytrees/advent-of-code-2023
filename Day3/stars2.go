package Day3

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func Star2() {
	input, _ := os.Open("Day3/input.txt")
	defer input.Close()

	scanner := bufio.NewScanner(input)

	schematic := newSchematic()

	n := 0
	sum := 0
	product := 0

	for scanner.Scan() {
		schematic.parseLine(scanner.Text())

		schematic.registerAdjacentSymbolsAndNumbers(n, n)

		if n > 0 {
			schematic.registerAdjacentSymbolsAndNumbers(n, n-1)
			schematic.registerAdjacentSymbolsAndNumbers(n-1, n)
		}

		n++
	}

	for _, l := range schematic.lines {
		for _, number := range l.numbers {
			if number.isPartNumber() {
				sum += number.value
			}
		}

		for _, symbol := range l.symbols {
			product += symbol.ratio()
		}
	}

	fmt.Println("536202")
	fmt.Println(sum)

	fmt.Println("78272573")
	fmt.Println(product)
}

type location struct {
	start int
	end   int
}

type number struct {
	location
	value           int
	adjacentSymbols map[*symbol]bool
}

func parseNumber(line string, offset []int) *number {
	value, _ := strconv.Atoi(line[offset[0]:offset[1]])

	return &number{
		location: location{
			start: offset[0],
			end:   offset[1],
		},
		value:           value,
		adjacentSymbols: map[*symbol]bool{},
	}
}

func (n *number) getBounds() []int {
	return []int{
		max(0, n.start-1),
		min(lineLength, n.end),
	}
}

func (n *number) isPartNumber() bool {
	return len(n.adjacentSymbols) > 0
}

type symbol struct {
	location
	value           byte
	adjacentNumbers map[*number]bool
}

func parseSymbol(line string, offset []int) *symbol {
	return &symbol{
		location: location{
			start: offset[0],
			end:   offset[1],
		},
		value:           line[offset[0]],
		adjacentNumbers: map[*number]bool{},
	}
}

func (s *symbol) isGear() bool {
	return s.value == '*'
}

func (s *symbol) ratio() int {
	result := 0

	if s.isGear() && len(s.adjacentNumbers) == 2 {
		result = 1

		for v := range s.adjacentNumbers {
			result *= v.value
		}
	}

	return result
}

func (s *symbol) overlaps(bounds []int) bool {
	return s.start <= bounds[1] && bounds[0] <= s.end-1
}

type line struct {
	numbers []*number
	symbols []*symbol
}

type schematic struct {
	lines        []*line
	numberRegexp *regexp.Regexp
	symbolRegexp *regexp.Regexp
}

func newSchematic() *schematic {
	return &schematic{
		lines:        []*line{},
		numberRegexp: regexp.MustCompile(`\d+`),
		symbolRegexp: regexp.MustCompile(`[^.0-9]`),
	}
}

func (s *schematic) parseLine(a string) {
	lineLength = len(a)
	l := &line{
		numbers: []*number{},
		symbols: []*symbol{},
	}

	partNumberOffsets := s.numberRegexp.FindAllStringIndex(a, -1)

	for _, numberOffset := range partNumberOffsets {
		l.numbers = append(l.numbers, parseNumber(a, numberOffset))
	}

	symbolOffsets := s.symbolRegexp.FindAllStringIndex(a, -1)

	for _, symbolOffset := range symbolOffsets {
		l.symbols = append(l.symbols, parseSymbol(a, symbolOffset))
	}

	s.lines = append(s.lines, l)
}

func (s *schematic) registerAdjacentSymbolsAndNumbers(n int, q int) {
	for _, number := range s.lines[n].numbers {
		for _, symbol := range s.lines[q].symbols {
			if symbol.overlaps(number.getBounds()) {
				symbol.adjacentNumbers[number] = true
				number.adjacentSymbols[symbol] = true
			}
		}
	}
}
